$(function () {
	var markdown = new showdown.Converter();

	$('.markdown-source').each( function () {
		var content = markdown.makeHtml( $(this).html() );

		$(this).parent().append('<div class="markdown-content">' + content + '</div>');
	});
});

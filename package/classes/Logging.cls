global class Logging
{
    public static final String FEATURE_NAME = 'Utilities_Logging';
    
    private static ILogger implementation;

	global interface ILogger
    {
        void log(String message);
        void log(String severity, String message);
        void log(String severity, String message, Id record);
        
        void write();
    }
    
    global static void setLogger(ILogger impl)
    {
        implementation = impl;
    }
    
    global static ILogger newLogger(String process)
    {
        return newLogger(process, false);
    }
    
    public static ILogger newLogger(String process, Boolean isNull)
    {
        if(implementation != null)
            return implementation;

        if(!Features.isActive(Logging.FEATURE_NAME))
            return (ILogger)new NullLogger();

        if(isNull)
        {
            return (ILogger)new NullLogger();
        }
        else
        {
            return (ILogger)new Logger(process);
        }
    }
    
    global class Logger implements Logging.ILogger
    {
        private Logging.Log logFile;
        
        public Logger(){}
        
        global Logger(String process)
        {
            this.logFile = new Logging.Log(process);

            Log__c record = this.logFile.toSObject();
            insert record;
            this.logFile = new Logging.Log(record);
        }
        
        global void log(String message)
        {
            log('Info', message);
        }

        global void log(String severity, String message)
        {
            log(severity, message, null);
        }

        global void log(String severity, String message, Id record)
        {
            Logging.LogItem line = new Logging.LogItem(severity, message, record);
            logFile.addLine(line);
        }
        
        global void write()
        {
            List<LogItem__c> lines = new List<LogItem__c>();
            for(LogItem line : logFile.LogItems)
            {
                lines.add( line.toSObject() );
            }
            insert lines;
            
            logFile.LogItems = new List<LogItem>();
        }
    }
    
    global class NullLogger implements Logging.ILogger
    {
        public NullLogger(){}

        global void log(String message) {}
        global void log(String severity, String message) {}
        global void log(String severity, String message, Id record) {}
        
        global void write() {}
    }
    
    global class Log
    {
        global Id Id {get; set;}
        global String Process {get; set;}
        global DateTime StartTime {get; set;}
        global List<LogItem> LogItems {get; set;}
        
        public Log(){}
        
        global Log(Log__c record)
        {
            this.Id = record.Id;
            this.Process = record.Process__c;
            this.StartTime = record.StartTime__c;
            this.LogItems = new List<LogItem>();
        }
        
        global Log(String process)
        {
            this.Process = process;
            this.StartTime = DateTime.now();
            this.LogItems = new List<LogItem>();
        }
        
        global Log__c toSObject()
        {
            return new Log__c(
                Id = this.Id,
                Process__c = this.Process,
                StartTime__c = this.StartTime
           	);
        }
        
        global void addLine(LogItem line)
        {
            line.Log = this.Id;
            this.LogItems.add(line);
        }
    }
    
    global class LogItem
    {
        global Id Id {get; set;}
        global Id Log {get; set;}
        global String Severity {get; set;}
        global String Message {get; set;}
        global Id Record {get; set;}
        global DateTime LogTime {get; set;}
        
        public LogItem(){}
        
        global LogItem(LogItem__c record)
        {
            this.Id = record.Id;
            this.Log = record.Log__c;
            this.Severity = record.Severity__c;
            this.Message = record.Message__c;
            this.Record = record.Record__c;
            this.LogTime = record.Time__c;
        }
        
        global LogItem(String severity, String message, Id record)
        {
            this.Severity = severity;
            this.Message = message;
            this.Record = record;
            this.LogTime = DateTime.now();
        }
        
        global LogItem__c toSObject()
        {
            return new LogItem__c(
                Id = this.Id,
            	Log__c = this.Log,
                Severity__c = this.Severity,
                Message__c = this.Message,
                Record__c = this.Record,
                Time__c = this.LogTime
            );
        }
    }
    
    global class MockLogger implements Logging.ILogger
    {
        Mocks.Method mockLog = new Mocks.Method('Logger', 'log');
        Mocks.Method mockWrite = new Mocks.Method('Logger', 'write');
        
        global void assertCalls()
        {
            mockLog.doAssertsNow();
            mockWrite.doAssertsNow();
        }

        global void log(String message)
        {
            mockLog.call(new List<Object>{message});
        }

        global void log(String severity, String message)
        {
            mockLog.call(new List<Object>{severity, message});
        }

        global void log(String severity, String message, Id record)
        {
            mockLog.call(new List<Object>{severity, message, record});
        }
        
        global Mocks.Method whenLog()
        {
            return mockLog;
        }
        
        global void write()
        {
            mockWrite.call();
        }
        
        global Mocks.Method whenWrite()
        {
            return mockWrite;
        }
    }
}
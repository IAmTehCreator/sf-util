@isTest(SeeAllData=false)
private class LogsControllerTest
{
    private static TestMethod void constructor_featureNotActive_returnsEarly()
    {
        FeaturesMock mock = newFeaturesMock();
        mock.whenIsActive().thenReturn(false)
            			   .thenAssertCalled(1)
            			   .thenAssertCalledWith('Utilities_Logging');
        
        Log__c log = new Log__c(Process__c = 'Test');
        ApexPages.StandardController standardController = new ApexPages.StandardController(log);
        LogsController controller = new LogsController(standardController);
        
        System.assertEquals(false, controller.IsFeatureEnabled, 'Should mark feature disabled');
        System.assertEquals(null, controller.Lines, 'Should not load the log lines');
        
        mock.assertCalls();
    }

    private static TestMethod void constructor_retrievesLogLines()
    {
        Log__c log = new Log__c(Process__c = 'Test');
        insert log;

        FeaturesMock serviceMock = newFeaturesMock();
        serviceMock.whenIsActive().thenReturn(true)
            					  .thenAssertCalled(1)
            					  .thenAssertCalledWith('Utilities_Logging');
        
        List<LogItem__c> lines = new List<LogItem__c>();
        lines.add(new LogItem__c(Log__c = log.Id, Message__c = 'Test1'));
        lines.add(new LogItem__c(Log__c = log.Id, Message__c = 'Test2'));
        
        GenericSObjectSelectorMock selectorMock = newSelectorMock();
        selectorMock.whenSelectWhere().thenReturn(lines)
            						  .thenAssertCalled(1)
            						  .thenAssertCalledWith('Log__c = \'' + log.Id + '\'');

        ApexPages.StandardController standardController = new ApexPages.StandardController(log);
        LogsController controller = new LogsController(standardController);
        
        System.assertEquals(true, controller.IsFeatureEnabled, 'Should mark feature enabled');
        System.assertEquals(2, controller.Lines.size(), 'Should load the log lines');
        System.assertEquals('Test1', controller.Lines.get(0).Message, 'Should create a DTO for the first line');
        System.assertEquals('Test2', controller.Lines.get(1).Message, 'Should create a DTO for the second line');
        
        serviceMock.assertCalls();
        selectorMock.assertCalls();
    }
    
    private static FeaturesMock newFeaturesMock()
    {
        FeaturesMock mock = new FeaturesMock();
        Features.setImplementation(mock);
        return mock;
    }
    
    private static GenericSObjectSelectorMock newSelectorMock()
    {
        GenericSObjectSelectorMock mock = new GenericSObjectSelectorMock();
        GenericSObjectSelector.setImplementation(mock);
        return mock;
    }
}
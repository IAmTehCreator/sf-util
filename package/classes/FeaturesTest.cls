@isTest(SeeAllData=true)
public class FeaturesTest
{
    private class TestException extends Exception {}

    private static final String TEST_FEATURE = 'Utilities_Test';

    private static TestMethod void features_isMockable()
    {
        FeaturesMock mock = new FeaturesMock();

        mock.whenIsActive().thenAssertCalled(1)
            			   .thenAssertCalledWith('Test');

        mock.whenCanActivate().thenAssertCalled(1)
            				  .thenAssertCalledWith('Test');

        mock.whenCanDeactivate().thenAssertCalled(1)
            					.thenAssertCalledWith('Test');

        mock.whenActivate().thenAssertCalled(1)
        				   .thenAssertCalledWith('Test');

        mock.whenDeactivate().thenAssertCalled(1)
            				 .thenAssertCalledWith('Test');

        mock.whenGetAllFeatures().thenAssertCalled(1);

        mock.whenGetFeature().thenAssertCalled(1)
            				 .thenAssertCalledWith('Test');

        mock.whenGetFeatureState().thenAssertCalled(1)
            					  .thenAssertCalledWith('Test');

        Features.setImplementation(mock);
        
        Features.isActive('Test');
        Features.canActivate('Test');
        Features.canDeactivate('Test');
        Features.activate('Test');
        Features.deactivate('Test');
        Features.getAllFeatures();
        Features.getFeature('Test');
        Features.getFeatureState('Test');
        
        mock.assertCalls();
    }
    
    private static TestMethod void features_canActivate_callsHandler()
    {
        Features.TestApexHandler handler = new Features.TestApexHandler();
        handler.whenBeforeActivate().thenAssertCalled(1);
        
        Features.FeatureActivationRequest request = Features.canActivate(TEST_FEATURE);
        
        System.assert(request.isSuccess(), 'Should allow activation');
        
        handler.assertCalls();
    }
    
    private static TestMethod void features_canActivate_handlerThrowsException()
    {
        Features.TestApexHandler handler = new Features.TestApexHandler();
        handler.whenBeforeActivate().thenThrow(new TestException('Error'));
        
        Features.FeatureActivationRequest request = Features.canActivate(TEST_FEATURE);
        
        System.assert(!request.isSuccess(), 'Should not allow activation');
        System.assertEquals('Error', request.getError(), 'Should set the exception message as the error message');
    }
    
    private static TestMethod void features_canDeactivate_callsHandler()
    {
        Features.TestApexHandler handler = new Features.TestApexHandler();
        handler.whenBeforeDeactivate().thenAssertCalled(1);
        
        Features.FeatureActivationRequest request = Features.canDeactivate(TEST_FEATURE);
        
        System.assert(request.isSuccess(), 'Should allow deactivation');
        
        handler.assertCalls();
    }
    
    private static TestMethod void features_canDeactivate_handlerThrowsException()
    {
        Features.TestApexHandler handler = new Features.TestApexHandler();
        handler.whenBeforeDeactivate().thenThrow(new TestException('Error'));
        
        Features.FeatureActivationRequest request = Features.canDeactivate(TEST_FEATURE);
        
        System.assert(!request.isSuccess(), 'Should not allow deactivation');
        System.assertEquals('Error', request.getError(), 'Should set the exception message as the error message');
    }
    
    private static TestMethod void features_activate_callsHandler()
    {
        Logging.MockLogger logger = new Logging.MockLogger();
        logger.whenWrite().thenAssertCalled(1);
        Logging.setLogger(logger);

        Features.TestApexHandler handler = new Features.TestApexHandler();
        handler.whenBeforeActivate().thenAssertCalled(1);
        handler.whenActivate().thenAssertCalled(1);
        
        Features.FeatureActivationRequest request = Features.activate(TEST_FEATURE);
        
        System.assert(request.isSuccess(), 'Should allow activation');
        handler.assertCalls();
        logger.assertCalls();
    }
    
    private static TestMethod void features_activate_beforeActivateFails()
    {
        Logging.MockLogger logger = new Logging.MockLogger();
        logger.whenWrite().thenAssertCalled(1);
        Logging.setLogger(logger);

        Features.TestApexHandler handler = new Features.TestApexHandler();
        handler.whenBeforeActivate().thenAssertCalled(1)
            						.thenDo(new FailActivationRequest('Error'));
        handler.whenActivate().thenAssertCalled(0);
        
        Features.FeatureActivationRequest request = Features.activate(TEST_FEATURE);
        
        System.assert(!request.isSuccess(), 'Should not allow activation');
        handler.assertCalls();
        logger.assertCalls();
    }
    
    private static TestMethod void features_activate_beforeActivateThrowsException()
    {
        Logging.MockLogger logger = new Logging.MockLogger();
        logger.whenWrite().thenAssertCalled(1);
        Logging.setLogger(logger);

        Features.TestApexHandler handler = new Features.TestApexHandler();
        handler.whenBeforeActivate().thenAssertCalled(1)
            						.thenThrow(new TestException('Error'));
        handler.whenActivate().thenAssertCalled(0);
        
        Features.FeatureActivationRequest request = Features.activate(TEST_FEATURE);
        
        System.assert(!request.isSuccess(), 'Should not allow activation');
        handler.assertCalls();
        logger.assertCalls();
    }
    
	private static TestMethod void features_deactivate_callsHandler()
    {
        Logging.MockLogger logger = new Logging.MockLogger();
        logger.whenWrite().thenAssertCalled(1);
        Logging.setLogger(logger);

        Features.TestApexHandler handler = new Features.TestApexHandler();
        handler.whenBeforeDeactivate().thenAssertCalled(1);
        handler.whenDeactivate().thenAssertCalled(1);
        
        Features.FeatureActivationRequest request = Features.deactivate(TEST_FEATURE);
        
        System.assert(request.isSuccess(), 'Should allow deactivation');
        handler.assertCalls();
        logger.assertCalls();
    }
    
    private static TestMethod void features_deactivate_beforeDeactivateFails()
    {
        Logging.MockLogger logger = new Logging.MockLogger();
        logger.whenWrite().thenAssertCalled(1);
        Logging.setLogger(logger);

        Features.TestApexHandler handler = new Features.TestApexHandler();
        handler.whenBeforeDeactivate().thenAssertCalled(1)
            						  .thenDo(new FailActivationRequest('Error'));
        handler.whenDeactivate().thenAssertCalled(0);
        
        Features.FeatureActivationRequest request = Features.deactivate(TEST_FEATURE);
        
        System.assert(!request.isSuccess(), 'Should not allow deactivation');
        handler.assertCalls();
        logger.assertCalls();
    }
    
    private static TestMethod void features_deactivate_beforeDeactivateThrowsException()
    {
        Logging.MockLogger logger = new Logging.MockLogger();
        logger.whenWrite().thenAssertCalled(1);
        Logging.setLogger(logger);

        Features.TestApexHandler handler = new Features.TestApexHandler();
        handler.whenBeforeDeactivate().thenAssertCalled(1)
            						  .thenThrow(new TestException('Error'));
        handler.whenDeactivate().thenAssertCalled(0);
        
        Features.FeatureActivationRequest request = Features.deactivate(TEST_FEATURE);
        
        System.assert(!request.isSuccess(), 'Should not allow deactivation');
        handler.assertCalls();
        logger.assertCalls();
    }
    
    private static TestMethod void features_getFeature_returnsFeatureAsDTO()
    {
        Feature__mdt metadata = [
            SELECT DeveloperName, Label, NamespacePrefix, Default__c, Description__c, ApexHandler__c, Locked__c, Hidden__c, Package__c
            	FROM Feature__mdt LIMIT 1
        ];
        
        if(metadata == null)
            return;
        
        Features.Feature feature = Features.getFeature(metadata.DeveloperName);
        
        System.assertEquals(metadata.Label, feature.Name, 'Should set the label');
        System.assertEquals(metadata.DeveloperName, feature.ApiName, 'Should set the API name');
        System.assertEquals(metadata.Default__c, feature.IsDefault, 'Should set default');
        System.assertEquals(metadata.Description__c, feature.Description, 'Should set description');
        System.assertEquals(metadata.ApexHandler__c, feature.ApexHandler, 'Should set Apex handler');
        System.assertEquals(metadata.Locked__c, feature.Locked, 'Should set locked');
        System.assertEquals(metadata.Hidden__c, feature.Hidden, 'Should set hidden');
        System.assertEquals(metadata.Package__c, feature.HostPackage, 'Should set package');
        System.assertEquals(metadata.NamespacePrefix, feature.Namespace, 'Should set namespace');
    }

    private static TestMethod void features_getAllFeatures_returnsAllFeatures()
    {
        List<Feature__mdt> metadata = [
            SELECT DeveloperName, Label, NamespacePrefix, Default__c, Description__c, ApexHandler__c, Locked__c, Hidden__c, Package__c
            	FROM Feature__mdt
        ];
        
        List<Features.Feature> dtos = Features.getAllFeatures();
        
        System.assertEquals(metadata.size(), dtos.size(), 'Should return all of the features');
    }

    private static TestMethod void featureActivationRequest_shouldReturnFeature()
    {
        Features.Feature feature = new Features.Feature();
        Features.FeatureActivationRequest request = new Features.FeatureActivationRequest(feature);
        
        System.assertEquals(feature, request.getFeature(), 'Should return feature');
    }

    private static TestMethod void featureActivationRequest_shouldReturnLogger()
    {
        Logging.MockLogger logger = newMockLogger();

        Features.Feature feature = new Features.Feature();
        Features.FeatureActivationRequest request = new Features.FeatureActivationRequest(feature);
        
        System.assertEquals(logger, request.getLogger(), 'Should return logger');
    }

    private static TestMethod void featureActivationRequest_shouldDefaultToSuccess()
    {
        Features.Feature feature = new Features.Feature();
        Features.FeatureActivationRequest request = new Features.FeatureActivationRequest(feature);
        
        System.assertEquals(true, request.isSuccess(), 'Should be success by default');
    }

    private static TestMethod void featureActivationRequest_fail_shouldNotReportSuccess()
    {
        Features.Feature feature = new Features.Feature();
        Features.FeatureActivationRequest request = new Features.FeatureActivationRequest(feature);
        
        request.fail('Error');
        
        System.assertEquals(false, request.isSuccess(), 'Should not be a success after a failure');
    }

    private static TestMethod void featureActivationRequest_fail_shouldStoreErrorMessage()
    {
        Features.Feature feature = new Features.Feature();
        Features.FeatureActivationRequest request = new Features.FeatureActivationRequest(feature);
        
        request.fail('Error');
        
        System.assertEquals('Error', request.getError(), 'Should return failure message');
    }

    private static TestMethod void featureActivationRequest_fail_shouldLogMessage()
    {
        Logging.MockLogger logger = newMockLogger();
        logger.whenLog().thenAssertCalled(1)
            			.thenAssertCalledWith(
                            new List<Object>{'Error', 'Feature Activation Request Failed: Error'}
                        );

        Features.Feature feature = new Features.Feature();
        Features.FeatureActivationRequest request = new Features.FeatureActivationRequest(feature);
        
        request.fail('Error');
        
        logger.assertCalls();
    }
    
    private static TestMethod void feature_getHandler_returnsApexHandler()
    {
        Features.Feature feature = new Features.Feature();
        feature.ApexHandler = 'FeaturesTest.ApexHandler';
        
        System.assert(feature.getHandler() instanceof FeaturesTest.ApexHandler, 'Should instantiate the specified apex class');
    }
    
    private static TestMethod void feature_getHandler_returnsNull()
    {
        Features.Feature feature = new Features.Feature();
        System.assertEquals(null, feature.getHandler(), 'Should return null');
    }
    
    private static TestMethod void feature_checkIsActive_updatesProperties()
    {
        Features.Feature feature = new Features.Feature();
        feature.ApiName = 'Test';
        
        Date testDate = Date.today();
        FeaturesMock mock = newMockFeatures();
        mock.whenGetFeatureState().thenReturn( newFeatureState('Test', true, testDate) )
            					  .thenAssertCalled(1)
            					  .thenAssertCalledWith('Test');
        
        System.assertEquals(true, feature.checkIsActive(), 'Should return the active state');
        System.assertEquals(true, feature.IsActive, 'Should mark the feature as active');
        System.assertEquals(testDate, feature.ActivationDate, 'Should set the activation date');
        
        mock.assertCalls();
    }
    
    public class ApexHandler implements Features.FeatureActivationHandler
    {
        public void beforeActivate(Features.FeatureActivationRequest request){}
        public void activate(Features.FeatureActivationRequest request){}
        public void beforeDeactivate(Features.FeatureActivationRequest request){}
        public void deactivate(Features.FeatureActivationRequest request){}
    }
    
    public class FailActivationRequest implements Mocks.MethodListener
    {
        public String FailureMessage {get; set;}
        
        public FailActivationRequest(String message)
        {
            FailureMessage = message;
        }

        public Object execute(List<Object> callArguments)
        {
            Features.FeatureActivationRequest request = (Features.FeatureActivationRequest)callArguments.get(0);
            request.fail(FailureMessage);

            return null;
        }
    }

	private static Logging.MockLogger newMockLogger()
    {
        Logging.MockLogger mock = new Logging.MockLogger();
        Logging.setLogger(mock);
        return mock;
    }
    
    private static FeaturesMock newMockFeatures()
    {
        FeaturesMock mock = new FeaturesMock();
        Features.setImplementation(mock);
        return mock;
    }
    
    private static FeatureActivationState__c newFeatureState(String feature, Boolean activated, Date activationDate)
    {
        return new FeatureActivationState__c(
            Feature__c = feature,
            Activated__c = activated,
            ActivationDate__c = activationDate
        );
    }
}
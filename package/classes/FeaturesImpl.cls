global class FeaturesImpl implements Features.API
{
	public FeaturesImpl() {}

    global Boolean isActive(String featureName)
    {
        if(Features.IsTest)
            return true;
        
        Features.Feature metadata = getFeature(featureName);
        FeatureActivationState__c state = getFeatureState(featureName);
        
        if(metadata.Locked)
        {
            //If the feature is locked and not in it's locked state then change it now
            if(metadata.IsDefault && state.Activated__c == false)
            {
                if(!Features.DisableDML)
                    activate(featureName, false);
                
                return true;
            }
            else if(!metadata.IsDefault && state.Activated__c == true)
            {
                if(!Features.DisableDML)
                    deactivate(featureName, false);
                
                return false;
            }
        }
        
        return state.Activated__c;
    }

    global Features.FeatureActivationRequest canActivate(String featureName)
    {
        Features.FeatureActivationRequest request = new Features.FeatureActivationRequest( getFeature(featureName) );
        
        if ( request.getFeature().Locked )
            request.fail('Feature activation is locked');
        
        try{
            Features.FeatureActivationHandler handler = request.getFeature().getHandler();
            
            if (handler != null)
                handler.beforeActivate(request);
        }
        catch(Exception e)
        {
            request.fail(e.getMessage());
        }
        
        return request;
    }
    
    global Features.FeatureActivationRequest canDeactivate(String featureName)
    {
        Features.FeatureActivationRequest request = new Features.FeatureActivationRequest( getFeature(featureName) );
        
        if ( request.getFeature().Locked )
            request.fail('Feature deactivation is locked');
        
        try{
            Features.FeatureActivationHandler handler = request.getFeature().getHandler();
            
            if (handler != null)
                handler.beforeDeactivate(request);
        }
        catch(Exception e)
        {
            request.fail(e.getMessage());
        }
        
        return request;
    }
    
    global Features.FeatureActivationRequest activate(String featureName)
    {
        return activate(featureName, true);
    }
    
    private Features.FeatureActivationRequest activate(String featureName, Boolean validate)
    {
        Features.FeatureActivationRequest request = new Features.FeatureActivationRequest( getFeature(featureName) );
        
        if (validate && request.getFeature().Locked)
        {
            request.fail('Feature activation is locked');
            return request;
        }
        
        try{
            Features.FeatureActivationHandler handler = request.getFeature().getHandler();
            if(handler != null)
            {
                if(validate)
                    handler.beforeActivate(request);
                
                if(request.isSuccess())
                    handler.activate(request);
            }
            
            if(request.isSuccess())
            {
                setFeatureState(request.getFeature(), true);
                request.getLogger().log(featureName + ' was successfully activated');
            }
        } catch(Exception e){
            request.fail(e.getMessage());
        }
        
        request.getLogger().write();
        return request;
    }
    
    global Features.FeatureActivationRequest deactivate(String featureName)
    {
        return deactivate(featureName, true);
    }
    
    public Features.FeatureActivationRequest deactivate(String featureName, Boolean validate)
    {
        Features.FeatureActivationRequest request = new Features.FeatureActivationRequest( getFeature(featureName) );
        
        if (validate && request.getFeature().Locked)
        {
            request.fail('Feature activation is locked');
            return request;
        }
        
        try{
            Features.FeatureActivationHandler handler = request.getFeature().getHandler();
            if (handler != null)
            {
                if(validate)
                    handler.beforeDeactivate(request);
                
                if (request.isSuccess())
                    handler.deactivate(request);
            }
            
            if(request.isSuccess())
            {
                setFeatureState(request.getFeature(), false);
                request.getLogger().log(featureName + ' was successfully deactivated');
            }
        } catch(Exception e){
            request.fail(e.getMessage());
        }
        
        request.getLogger().write();
        return request;
    }
    
    global List<Features.Feature> getAllFeatures()
    {
        List<Feature__mdt> metadata = [
            SELECT DeveloperName, Label, NamespacePrefix, Default__c, Description__c, ApexHandler__c, Locked__c, Hidden__c, Package__c
            FROM Feature__mdt
        ];
        
        List<Features.Feature> dtos = new List<Features.Feature>();
        for(Feature__mdt feature : metadata)
        {
            dtos.add( new Features.Feature(feature) );
        }
        
        return dtos;
    }
    
    global Features.Feature getFeature(String featureName)
    {
        Feature__mdt metadata = [
            SELECT DeveloperName, Label, NamespacePrefix, Default__c, Description__c, ApexHandler__c, Locked__c, Hidden__c, Package__c
            FROM Feature__mdt
            WHERE DeveloperName = :featureName LIMIT 1
        ];
        
        return new Features.Feature(metadata, true);
    }
    
    private void setFeatureState(Features.Feature feature, Boolean active)
    {
        FeatureActivationState__c state = getFeatureState(feature.ApiName);
        state.Activated__c = active;
        state.ActivationDate__c = active ? Date.today() : null;
        
        upsert state;
    }
    
    global FeatureActivationState__c getFeatureState(String apiName)
    {
        FeatureActivationState__c state = FeatureActivationState__c.getValues(apiName);
        if(state == null)
        {
            state = new FeatureActivationState__c(
                Name = apiName,
                Feature__c = apiName
            );
            
            if(!Features.DisableDML)
                insert state;
        }
        
        return state;
    }
}
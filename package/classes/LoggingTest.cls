@isTest(SeeAllData=false)
private class LoggingTest
{
    private static TestMethod void service_isMockable()
    {
        Account acc = new Account(Name = 'Test');
        insert acc;

        Logging.MockLogger mock = new Logging.MockLogger();
        
        mock.whenLog().thenAssertCalled(3)
            		  .thenAssertCalledWith('message')
            		  .thenAssertCalledWith(new List<Object>{'Warning', 'message'})
            		  .thenAssertCalledWith(new List<Object>{'Error', 'message', acc.Id});
        
        mock.whenWrite().thenAssertCalled(1);

        Logging.setLogger(mock);
        
        Logging.ILogger logger = Logging.newLogger('Test');
        System.assertEquals(mock, logger, 'Should return specified implementation');
        
        logger.log('message');
        logger.log('Warning', 'message');
        logger.log('Error', 'message', acc.Id);
        logger.write();
        
        mock.assertCalls();
    }

    private static TestMethod void service_newLogger_checksFeatureActivation()
    {
        Logging.ILogger logger = Logging.newLogger('Test');
        System.assert(logger instanceof Logging.NullLogger, 'Should return a Null logger when feature is inactive');
    }

    private static TestMethod void service_newLogger_returnsNewLogger()
    {
        Features.IsTest = true;
        Logging.ILogger logger = Logging.newLogger('Test');
        
        System.assert(logger instanceof Logging.Logger, 'Should return a Logging.Logger');
    }

    private static TestMethod void service_newLogger_returnsNullLogger()
    {
        Features.IsTest = true;
        Logging.ILogger logger = Logging.newLogger('Test', true);
        
        System.assert(logger instanceof Logging.NullLogger, 'Should return a Logging.NullLogger');
    }
    
    private static TestMethod void nullLogger_doesNotLog()
    {
        Account relatedRecord = new Account(Name = 'Test');
        insert relatedRecord;

        Features.IsTest = true;
        Logging.ILogger logger = Logging.newLogger('Test', true);
        
        logger.log('Test');
        logger.log('Error', 'Test error');
        logger.log('Warning', 'Message with related record', relatedRecord.Id);
        
        Integer dml = Limits.getDmlStatements();
        logger.write();
        
        System.assertEquals(dml, Limits.getDmlStatements(), 'Should not write logs to the database');
    }
    
    private static TestMethod void logger_logsInfoMessages()
    {
        Features.IsTest = true;
        Logging.ILogger logger = Logging.newLogger('Test');
        
        logger.log('Test1');
        logger.log('Test2');
        logger.write();
        
        Log__c log = [SELECT Id, Name, Process__c FROM Log__c WHERE Process__c = 'Test'];
        List<LogItem__c> logItems = [
            SELECT Id, Name, Log__c, Severity__c, Message__c, Record__c, Time__c
            	FROM LogItem__c
            	WHERE Log__c = :log.Id
            	ORDER BY Message__c ASC
        ];
        
        System.assertEquals('Test', log.Process__c, 'Should create the log file');
        System.assertEquals(2, logItems.size(), 'Should create two log records');
        
        System.assertEquals('Test1', logItems[0].Message__c, 'Should set the message');
        System.assertEquals('Test2', logItems[1].Message__c, 'Should set the message');
    }

    private static TestMethod void logger_logsMessageWithSeverity()
    {
        Features.IsTest = true;
        Logging.ILogger logger = Logging.newLogger('Test');
        
        logger.log('Info', 'Test1');
        logger.log('Error', 'Test2');
        logger.write();
        
        Log__c log = [SELECT Id, Name, Process__c FROM Log__c WHERE Process__c = 'Test'];
        List<LogItem__c> logItems = [
            SELECT Id, Name, Log__c, Severity__c, Message__c, Record__c, Time__c
            	FROM LogItem__c
            	WHERE Log__c = :log.Id
            	ORDER BY Message__c ASC
        ];
        
        System.assertEquals('Test', log.Process__c, 'Should create the log file');
        System.assertEquals(2, logItems.size(), 'Should create two log records');
        
        System.assertEquals('Test1', logItems[0].Message__c, 'Should set the message of first log');
        System.assertEquals('Info', logItems[0].Severity__c, 'Should set the severity of first log');
        System.assertEquals('Test2', logItems[1].Message__c, 'Should set the message of second log');
        System.assertEquals('Error', logItems[1].Severity__c, 'Should set the severity of second log');
    }

    private static TestMethod void logger_logsMessageWithRelatedRecord()
    {
        Account relatedRecord = new Account(Name = 'Test');
        insert relatedRecord;
        Id relatedId = relatedRecord.Id;

        Features.IsTest = true;
        Logging.ILogger logger = Logging.newLogger('Test');
        
        logger.log('Info', 'Test1', relatedId);
        logger.log('Error', 'Test2', relatedId);
        logger.write();
        
        Log__c log = [SELECT Id, Name, Process__c FROM Log__c WHERE Process__c = 'Test'];
        List<LogItem__c> logItems = [
            SELECT Id, Name, Log__c, Severity__c, Message__c, Record__c, Time__c
            	FROM LogItem__c
            	WHERE Log__c = :log.Id
            	ORDER BY Message__c ASC
        ];
        
        System.assertEquals('Test', log.Process__c, 'Should create the log file');
        System.assertEquals(2, logItems.size(), 'Should create two log records');
        
        System.assertEquals('Test1', logItems[0].Message__c, 'Should set the message of first log');
        System.assertEquals('Info', logItems[0].Severity__c, 'Should set the severity of first log');
        System.assertEquals(relatedId, logItems[0].Record__c, 'Should set the related record ID of first log');
        System.assertEquals('Test2', logItems[1].Message__c, 'Should set the message of second log');
        System.assertEquals('Error', logItems[1].Severity__c, 'Should set the severity of second log');
        System.assertEquals(relatedId, logItems[1].Record__c, 'Should set the related record ID of second log');
    }

    private static TestMethod void log_constructsFromRecord()
    {
        Log__c log = new Log__c(
            Process__c = 'Test',
            StartTime__c = DateTime.now()
        );
        insert log;
        
        Logging.Log dto = new Logging.Log(log);
        
        System.assertEquals(log.Id, dto.Id, 'Should copy the ID from the record');
        System.assertEquals(log.Process__c, dto.Process, 'Should copy the process name from the record');
        System.assertEquals(log.StartTime__c, dto.StartTime, 'Should copy the start time from the record');
        System.assertEquals(new List<Logging.LogItem>(), dto.LogItems, 'Should initialise the log items list');
    }

    private static TestMethod void log_constructsFromProcessName()
    {
        Logging.Log dto = new Logging.Log('Test');
        
        System.assertEquals('Test', dto.Process, 'Should set the process name');
        System.assertEquals(new List<Logging.LogItem>(), dto.LogItems, 'Should initialise the log items list');
    }

    private static TestMethod void log_toSObjct_returnsRecord()
    {
        Log__c log = new Log__c(
            Process__c = 'Test',
            StartTime__c = DateTime.now()
        );
        insert log;
        
        DateTime testTime = DateTime.now();
        Logging.Log dto = new Logging.Log(log);
        dto.Process = 'Test2';
        dto.StartTime = testTime;

        Log__c record = dto.toSObject();
        
        System.assertEquals(dto.Id, record.Id, 'Should copy the ID');
        System.assertEquals(dto.Process, record.Process__c, 'Should copy the updated process name');
        System.assertEquals(testTime, record.StartTime__c, 'Should copy the updated start time');
    }
    
    private static TestMethod void log_addLine_addsLogItem()
    {
        Log__c logRecord = new Log__c(Process__c = 'Test');
        insert logRecord;

        Logging.Log dto = new Logging.Log(logRecord);
        Logging.LogItem line = new Logging.LogItem('Info', 'Test Log Item', null);
        dto.addLine(line);
        
        System.assertEquals(1, dto.LogItems.size(), 'Should add one log item');
        System.assertEquals(line, dto.LogItems.get(0), 'Should add the provided log item to the list');
        System.assertEquals(logRecord.Id, line.Log, 'Should set the log ID on the log item');
    }
    
    private static TestMethod void logItem_constructsFromRecord()
    {
        Account relatedRecord = new Account(Name = 'Test');
        insert relatedRecord;

        Log__c log = new Log__c(Process__c = 'Test');
        insert log;

        LogItem__c record = new LogItem__c(
            Log__c = log.Id,
            Severity__c = 'Info',
            Message__c = 'A test log',
            Record__c = relatedRecord.Id,
            Time__c = DateTime.now()
        );
        insert record;
        
        Logging.LogItem dto = new Logging.LogItem(record);
        
        System.assertEquals(record.Id, dto.Id, 'Should copy the record ID');
        System.assertEquals(record.Log__c, dto.Log, 'Should copy the Log ID');
        System.assertEquals(record.Severity__c, dto.Severity, 'Should copy the severity');
        System.assertEquals(record.Message__c, dto.Message, 'Should copy the message');
        System.assertEquals(record.Record__c, dto.Record, 'Should copy the related record ID');
        System.assertEquals(record.Time__c, dto.LogTime, 'Should copy the time');
    }

    private static TestMethod void logItem_constructsFromMessage()
    {
        Account relatedRecord = new Account(Name = 'Test');
        insert relatedRecord;
        
        Logging.LogItem dto = new Logging.LogItem('Info', 'Test message', relatedRecord.Id);
        
        System.assertEquals('Info', dto.Severity, 'Should copy the severity');
        System.assertEquals('Test message', dto.Message, 'Should copy the message');
        System.assertEquals(relatedRecord.Id, dto.Record, 'Should copy the related record ID');
    }

    private static TestMethod void logItem_toSObject_returnsRecord()
    {
        Account relatedRecord = new Account(Name = 'Test');
        insert relatedRecord;

        Log__c logRecord = new Log__c(Process__c = 'Test');
        insert logRecord;
        
        LogItem__c line = new LogItem__c(
            Log__c = logRecord.Id,
            Severity__c = 'Error',
            Message__c = 'Test error',
            Record__c = relatedRecord.Id,
            Time__c = DateTime.now()
        );
        insert line;
        
        Logging.LogItem dto = new Logging.LogItem(line);
        LogItem__c record = dto.toSObject();
        
        System.assertEquals(dto.Id, record.Id, 'Should copy the ID');
        System.assertEquals(dto.Log, record.Log__c, 'Should copy the parent log ID');
        System.assertEquals(dto.Severity, record.Severity__c, 'Should copy the severity');
        System.assertEquals(dto.Message, record.Message__c, 'Should copy the message');
        System.assertEquals(dto.Record, record.Record__c, 'Should copy the related record ID');
        System.assertEquals(dto.LogTime, record.Time__c, 'Should copy the log time');
    }
}
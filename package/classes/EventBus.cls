global class EventBus 
{
    global class EventException extends Exception {}
    
    /**
     * Constructs a new event to invoke a callback function.
	 * @param {EventBus.Callback} fn The callback to invoke
	 * @param {List<Object>} args The arguments to be given to the 
     */
    global static EventBus.CallbackInvoker invoke(EventBus.Callback fn)
    {
        return new EventBus.CallbackInvoker(fn);
    }
    
    /**
     * @class CallbackInvoker
     * This class is used to build an argument map and event to invoke an event callback.
     */
    global class CallbackInvoker
    {
        private EventBus.Callback fn;
        private Map<String, Object> args;
        
        public CallbackInvoker(){}
        public CallbackInvoker(EventBus.Callback func)
        {
            fn = func;
            args = new Map<String, Object>();
        }
        
        /**
         * Adds an argument to the argument map to be provided to the callback function.
		 * @param {String} argName The name of the argument
		 * @param {Object} argValue The value of the argument
		 * @return {EventBus.CallbackInvoker} This method is chainable
		 */
        global EventBus.CallbackInvoker withArgument(String argName, Object argValue)
        {
            args.put(argName, argValue);
            return this;
        }
        
        /**
         * Invokes the method with all of the arguments provided so far and returns the event
         * used to invoke the method with.
		 * @return {EventBus.Event} The event used to invoke the callback with
         */
        global EventBus.Event now()
        {
            EventBus.event event = new EventBus.Event('invoke', args);
            
            try
            {
                fn.call(event);
            }
            catch(Exception e)
            {
                event.registerError(e.getMessage());
            }
            
            return event;
        }
    }
    
    global interface Callback
    {
        void call(EventBus.Event event);
    }

    /**
     * @class Event
     * Represents a fired event.
	 * @property {String} Name The name of the event invoked
	 * @property {Map<String, Object>} Arguments The arguments passed with this invocation
	 * @property {List<String>} Errors A list of error messages caused by the listeners of this event
	 * @property {Boolean} Veto True if this event has been terminated
     */
    global class Event
    {
        global String Name {get; private set;}
        global Map<String, Object> Arguments {get; private set;}
        global List<String> Errors {get; set;}
        global Boolean Veto {get; set;}
        
        public Event(){}
        public Event(String eventName, Map<String, Object> args)
        {
            Name = eventName;
            Arguments = args;
            Errors = new List<String>();
            Veto = false;
        }
        
        global Set<String> getArgumentNames()
        {
            return Arguments.keySet();
        }
        
        global void terminate()
        {
            Veto = true;
        }
        
        global Boolean hasErrors()
        {
            return Errors.size() >= 1;
        }
        
        global void registerError(String errorMessage)
        {
            Errors.add(errorMessage);
        }
        
        global Boolean isTerminated()
        {
            return Veto;
        }
    }
    
    global abstract class Emitter
    {   
        private List<Callback> listeners = new List<Callback>();

        global EventBus.Event fire(List<Object> args)
        {
            List<String> argNames = getArgumentNames();
            Map<String, Object> arguments = new Map<String, Object>();
            
            if(args.size() != argNames.size())
            {
				throw new EventException('Invalid number of arguments passed. Expected ' + argNames.size() + ' but got ' + args.size() + '.');
            }

            for(Integer i = 0; i < argNames.size(); i++)
            {
                arguments.put(argNames[i], args[i]);
            }

            EventBus.Event event = new EventBus.Event(getEventName(), arguments);

            for(Callback listener : listeners)
            {
                try
                {
                	listener.call(event);
                	if(event.isTerminated())
                    	break;
                }
                catch(Exception e)
                {
                    event.registerError(e.getMessage());
                }
            }
            
            return event;
        }
        
        global void addListener(EventBus.Callback listener)
        {
            listeners.add(listener);
        }
        
        global abstract List<String> getArgumentNames();
        global abstract String getEventName();
    }

	global virtual class Observable
    {
        private Map<String, EventBus.Emitter> eventEmitters = new Map<String, EventBus.Emitter>();
        
        global virtual void registerEvent(EventBus.Emitter event)
        {
            eventEmitters.put(event.getEventName(), event);
        }
        
        global virtual void registerEvents(List<EventBus.Emitter> events)
        {
            for(EventBus.Emitter event : events)
            {
				registerEvent(event);
            }
        }
        
        global void addEventListener(String eventName, Callback fn)
        {
            EventBus.Emitter event = eventEmitters.get(eventName);
            if(event != null)
                event.addListener(fn);
        }
        
        global virtual EventBus.Event fireEvent(String eventName)
        {
            return fireEvent(eventName, new List<Object>());
        }
        
        global virtual EventBus.Event fireEvent(String eventName, List<Object> args)
        {
            EventBus.Emitter event = eventEmitters.get(eventName);
            if(event != null)
                return event.fire(args);
            
            return null;
        }
        
        global Set<String> getEvents()
        {
			return eventEmitters.keySet();
        }
        
        global virtual void removeEvent(String eventName)
        {
            eventEmitters.remove(eventName);
        }
    }
    
    /**
     * @class EventBuilder
     * Helper class to construct events for unit tests.
     */
    global class EventBuilder
    {
        private String eventName;
        private Map<String, Object> eventArgs = new Map<String, Object>();
        
        global EventBus.EventBuilder withName(String name)
        {
            eventName = name;
            return this;
        }
        
        global EventBus.EventBuilder withArgument(String argName, Object argValue)
        {
            eventArgs.put(argName, argValue);
            return this;
        }
        
        global EventBus.Event fire()
        {
            return new EventBus.Event(eventName, eventArgs);
        }
    }
    
    /**
     * @class MockCallback
     * Utility class that can be used in place of a real callback on an observable event which can
     * be used in unit tests to test an observable.
	 * @property {List<EventBus.Event>} Calls Stores every event object that this callback was called with
	 * @property {Boolean} Veto When true this callback will veto each event it is invoked with
	 * @property {Boolean} ThrowException When true this callback will throw an exception each time it is
	 * 									  invoked, the exception message is 'Test Error'.
     */
    global class MockCallback implements EventBus.Callback
    {
        global List<EventBus.Event> Calls = new List<EventBus.Event>();
        global Boolean Veto = false;
        global Boolean ThrowException = false;

        global void call(EventBus.Event event)
        {
            Calls.add(event);
            
            if(Veto)
                event.terminate();
            
            if(ThrowException)
                throw new EventBus.EventException('Test Error');
        }
    }
}
global class Features
{
    //Class settings
    public static Boolean EnableLogging = true;
    public static Boolean DisableDML = false;
    public static Boolean IsTest = false;
    
    global interface API
    {
        Boolean isActive(String featureName);

        Features.FeatureActivationRequest canActivate(String featureName);
        Features.FeatureActivationRequest canDeactivate(String featureName);

        Features.FeatureActivationRequest activate(String featureName);
        Features.FeatureActivationRequest deactivate(String featureName);

        List<Features.Feature> getAllFeatures();
        Features.Feature getFeature(String featureName);
        FeatureActivationState__c getFeatureState(String apiName);
    }
    
    private static Features.API implementation;
    
    private static Features.API service()
    {
        return implementation == null ? (Features.API)new FeaturesImpl() : implementation;
    }
    
    global static void setImplementation(Features.API impl)
    {
        implementation = impl;
    }
    
    /**
	 * Determines if a feature has been activated.
	 * @param {String} featureName The name of the feature to check if active
	 * @return {Boolean} True if the feature is currently active
	 */
    global static Boolean isActive(String featureName)
    {
        return service().isActive(featureName);
    }
    
    /**
	 * Determines if the specified feature can be activated. This will call the beforeActivate
	 * method of the apex handler to determine if the feature can be activated.
	 * @param {String} featureName The name of the feature to be validated for activation
	 * @return {Features.FeatureActivationRequest} The request object
	 */
    global static Features.FeatureActivationRequest canActivate(String featureName)
    {
        return service().canActivate(featureName);
    }
    
    global static Features.FeatureActivationRequest canDeactivate(String featureName)
    {
        return service().canDeactivate(featureName);
    }
    
    global static Features.FeatureActivationRequest activate(String featureName)
    {
        return service().activate(featureName);
    }
    
    global static Features.FeatureActivationRequest deactivate(String featureName)
    {
        return service().deactivate(featureName);
    }
    
    global static List<Features.Feature> getAllFeatures()
    {
        return service().getAllFeatures();
    }
    
    global static Features.Feature getFeature(String featureName)
    {
        return service().getFeature(featureName);
    }
    
    global static FeatureActivationState__c getFeatureState(String apiName)
    {
        return service().getFeatureState(apiName);
    }
    
    global interface FeatureActivationHandler
    {
        void beforeActivate(Features.FeatureActivationRequest request);
        void activate(Features.FeatureActivationRequest request);
        
        void beforeDeactivate(Features.FeatureActivationRequest request);
        void deactivate(Features.FeatureActivationRequest request);
    }
    
    global class FeatureActivationRequest
    {
        private Features.Feature feature;
        private Boolean success;
        private String error;
        private Logging.ILogger logger;
        
        global FeatureActivationRequest(Features.Feature feature)
        {
            this.feature = feature;
            this.success = true;
            this.logger = Logging.newLogger('FeatureActivationRequest', !Features.EnableLogging);
        }
        
        global Features.Feature getFeature()
        {
            return this.feature;
        }
        
        global Logging.ILogger getLogger()
        {
            return this.logger;
        }
        
        global String getError()
        {
            return this.error;
        }
        
        global Boolean isSuccess()
        {
            return this.success;
        }
        
        global void fail(String message)
        {
            this.success = false;
            this.error = message;
            this.logger.log('Error', 'Feature Activation Request Failed: ' + message);
        }
    }
    
    global class Feature
    {
        global String Name {global get; public set;}
        global String ApiName {global get; public set;}
        global Boolean IsDefault {global get; public set;}
        global String Description {global get; public set;}
        global String ApexHandler {global get; public set;}
        global Boolean Locked {global get; public set;}
        global Boolean Hidden {global get; public set;}
        global String HostPackage {global get; public set;}
        global String Namespace {global get; public set;}
        global Boolean IsActive {global get; public set;}
        global Date ActivationDate {global get; public set;}
        
        public Feature() {}
        
        global Feature(Feature__mdt metadata)
        {
            this(metadata, false);
        }
        
        global Feature(Feature__mdt metadata, Boolean checkActive)
        {
            this(metadata, checkActive ? Features.getFeatureState(metadata.DeveloperName) : null);
        }
        
        global Feature(Feature__mdt metadata, FeatureActivationState__c state)
        {
            this.Name = metadata.Label;
            this.ApiName = metadata.DeveloperName;
            this.IsDefault = metadata.Default__c;
            this.Description = metadata.Description__c;
            this.ApexHandler = metadata.ApexHandler__c;
            this.Locked = metadata.Locked__c;
            this.Hidden = metadata.Hidden__c;
            this.HostPackage = metadata.Package__c;
            this.Namespace = metadata.NamespacePrefix;
            
            if(state != null)
            {
                this.IsActive = state.Activated__c;
                this.ActivationDate = state.ActivationDate__c;
            }
        }
        
        global Features.FeatureActivationHandler getHandler()
        {
            if (String.isBlank(this.ApexHandler))
                return null;

            Type handlerType = Type.forName(this.ApexHandler);
            
            return (Features.FeatureActivationHandler)handlerType.newInstance();
        }
        
        global Boolean checkIsActive()
        {
            FeatureActivationState__c state = Features.getFeatureState(this.ApiName);
            this.IsActive = state.Activated__c;
            this.ActivationDate = state.ActivationDate__c;

            return this.IsActive;
        }
    }
    
    private static TestApexHandler testHandler;
    global class TestApexHandler implements Features.FeatureActivationHandler
    {
        private Mocks.Method mockBeforeActivate = new Mocks.Method('ApexHandler', 'beforeActivate');
        private Mocks.Method mockActivate = new Mocks.Method('ApexHandler', 'activate');
        private Mocks.Method mockBeforeDeactivate = new Mocks.Method('ApexHandler', 'beforeDeactivate');
        private Mocks.Method mockDeactivate = new Mocks.Method('ApexHandler', 'deactivate');
        
        global TestApexHandler()
        {
            if(Features.testHandler == null)
            {
                Features.testHandler = this;
            }
            else
            {
                mockBeforeActivate = Features.testHandler.whenBeforeActivate();
                mockActivate = Features.testHandler.whenActivate();
                mockBeforeDeactivate = Features.testHandler.whenBeforeDeactivate();
                mockDeactivate = Features.testHandler.whenDeactivate();
            }
        }
        
        global void assertCalls()
        {
            mockBeforeActivate.doAssertsNow();
            mockActivate.doAssertsNow();
            mockBeforeDeactivate.doAssertsNow();
            mockDeactivate.doAssertsNow();
        }

        global void beforeActivate(Features.FeatureActivationRequest request)
        {
            mockBeforeActivate.call(new List<Object>{request});
        }
        
        global Mocks.Method whenBeforeActivate()
        {
            return mockBeforeActivate;
        }

        global void activate(Features.FeatureActivationRequest request)
        {
            mockActivate.call(new List<Object>{request});
        }
        
        global Mocks.Method whenActivate()
        {
            return mockActivate;
        }
        
        global void beforeDeactivate(Features.FeatureActivationRequest request)
        {
            mockBeforeDeactivate.call(new List<Object>{request});
        }
        
        global Mocks.Method whenBeforeDeactivate()
        {
            return mockBeforeDeactivate;
        }

        global void deactivate(Features.FeatureActivationRequest request)
        {
            mockDeactivate.call(new List<Object>{request});
        }
        
        global Mocks.Method whenDeactivate()
        {
            return mockDeactivate;
        }
    }
}
global class FeaturesMock implements Features.API
{
    private Mocks.Method mockIsActive = new Mocks.Method('Features', 'isActive');
    private Mocks.Method mockCanActivate = new Mocks.Method('Features', 'canActivate');
    private Mocks.Method mockCanDeactivate = new Mocks.Method('Features', 'canDeactivate');
    private Mocks.Method mockActivate = new Mocks.Method('Features', 'activate');
    private Mocks.Method mockDeactivate = new Mocks.Method('Features', 'deactivate');
    private Mocks.Method mockGetAllFeatures = new Mocks.Method('Features', 'getAllFeatures');
    private Mocks.Method mockGetFeature = new Mocks.Method('Features', 'getFeature');
    private Mocks.Method mockGetFeatureState = new Mocks.Method('Features', 'getFeatureState');
    
    global void assertCalls()
    {
        mockIsActive.doAssertsNow();
        mockCanActivate.doAssertsNow();
        mockCanDeactivate.doAssertsNow();
        mockActivate.doAssertsNow();
        mockDeactivate.doAssertsNow();
        mockGetAllFeatures.doAssertsNow();
        mockGetFeature.doAssertsNow();
        mockGetFeatureState.doAssertsNow();
    }

	global Boolean isActive(String featureName)
    {
        return (Boolean)mockIsActive.call(new List<Object>{featureName});
    }
    
    global Mocks.Method whenIsActive()
    {
        return mockIsActive;
    }

    global Features.FeatureActivationRequest canActivate(String featureName)
    {
        return (Features.FeatureActivationRequest)mockCanActivate.call(new List<Object>{featureName});
    }
    
    global Mocks.Method whenCanActivate()
    {
        return mockCanActivate;
    }

    global Features.FeatureActivationRequest canDeactivate(String featureName)
    {
        return (Features.FeatureActivationRequest)mockCanDeactivate.call(new List<Object>{featureName});
    }
    
    global Mocks.Method whenCanDeactivate()
    {
        return mockCanDeactivate;
    }
    
    global Features.FeatureActivationRequest activate(String featureName)
    {
        return (Features.FeatureActivationRequest)mockActivate.call(new List<Object>{featureName});
    }
    
    global Mocks.Method whenActivate()
    {
        return mockActivate;
    }

    global Features.FeatureActivationRequest deactivate(String featureName)
    {
        return (Features.FeatureActivationRequest)mockDeactivate.call(new List<Object>{featureName});
    }
    
    global Mocks.Method whenDeactivate()
    {
        return mockDeactivate;
    }
    
    global List<Features.Feature> getAllFeatures()
    {
        return (List<Features.Feature>)mockGetAllFeatures.call();
    }
    
    global Mocks.Method whenGetAllFeatures()
    {
        return mockGetAllFeatures;
    }

    global Features.Feature getFeature(String featureName)
    {
        return (Features.Feature)mockGetFeature.call(new List<Object>{featureName});
    }
    
    global Mocks.Method whenGetFeature()
    {
        return mockGetFeature;
    }

    global FeatureActivationState__c getFeatureState(String apiName)
    {
        return (FeatureActivationState__c)mockGetFeatureState.call(new List<Object>{apiName});
    }
    
    global Mocks.Method whenGetFeatureState()
    {
        return mockGetFeatureState;
    }
}
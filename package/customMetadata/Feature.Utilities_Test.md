<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Test</label>
    <protected>true</protected>
    <values>
        <field>ApexHandler__c</field>
        <value xsi:type="xsd:string">Features.TestApexHandler</value>
    </values>
    <values>
        <field>Default__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">A feature that can be used for unit testing. Activating this feature will do nothing.</value>
    </values>
    <values>
        <field>Hidden__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Locked__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Package__c</field>
        <value xsi:type="xsd:string">Utilities</value>
    </values>
</CustomMetadata>

public class LogsController
{
    private ApexPages.StandardController standardController;
    
    public List<Logging.LogItem> Lines {get; set;}
    public Boolean IsFeatureEnabled {public get; private set;}

	public LogsController(ApexPages.StandardController stdController)
    {
        standardController = stdController;
        Id logId = standardController.getId();
        
        Features.DisableDML = true;
        IsFeatureEnabled = Features.isActive(Logging.FEATURE_NAME);
        Features.DisableDML = false;
        if(!IsFeatureEnabled)
            return;
        
        List<LogItem__c> records = GenericSObjectSelector.newInstance(LogItem__c.SObjectType)
            .selectWhere('Log__c = \'' + logId + '\'');
        
        Lines = new List<Logging.LogItem>();
        for(LogItem__c record : records)
        {
            Lines.add( new Logging.LogItem(record) );
        }
    }
}
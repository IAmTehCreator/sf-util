<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Logging</label>
    <protected>true</protected>
    <values>
        <field>ApexHandler__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Default__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Provides a utility class that enables others to log messages to the database.</value>
    </values>
    <values>
        <field>Hidden__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Locked__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Package__c</field>
        <value xsi:type="xsd:string">Utilities</value>
    </values>
</CustomMetadata>

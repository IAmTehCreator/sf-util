global class GenericSObjectSelectorMock implements GenericSObjectSelector.API
{
	private Mocks.Method mockSelectById = new Mocks.Method('GenericSObjectSelector', 'selectById');
    private Mocks.Method mockSelectByIds = new Mocks.Method('GenericSObjectSelector', 'selectByIds');
    private Mocks.Method mockSelectWhere = new Mocks.Method('GenericSObjectSelector', 'selectWhere');
    private Mocks.Method mockSelectAll = new Mocks.Method('GenericSObjectSelector', 'selectAll');
    
    global void assertCalls()
    {
        mockSelectById.doAssertsNow();
        mockSelectByIds.doAssertsNow();
        mockSelectWhere.doAssertsNow();
        mockSelectAll.doAssertsNow();
    }
    
    global SObject selectById(Id recordId)
    {
        return (SObject)mockSelectById.call(new List<Object>{recordId});
    }
    
    global Mocks.Method whenSelectById()
    {
        return mockSelectById;
    }
    
    global List<SObject> selectByIds(Set<Id> ids)
    {
        return (List<SObject>)mockSelectByIds.call(new List<Object>{ids});
    }
    
    global Mocks.Method whenSelectByIds()
    {
        return mockSelectByIds;
    }
    
    global List<SObject> selectWhere(String whereClause)
    {
        return (List<SObject>)mockSelectWhere.call(new List<Object>{whereClause});
    }
    
    global Mocks.Method whenSelectWhere()
    {
        return mockSelectWhere;
    }
    
    global List<SObject> selectAll()
    {
        return (List<SObject>)mockSelectAll.call();
    }
    
    global Mocks.Method whenSelectAll()
    {
        return mockSelectAll;
    }
}
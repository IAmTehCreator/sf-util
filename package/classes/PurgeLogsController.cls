public class PurgeLogsController
{
    private ApexPages.StandardSetController standardController;

    public List<Log__c> Records {public get; private set;}
    public List<Log__c> SelectedRecords {public get; private set;}

    public String FilterType {public get; public set;}
    public String FilterProcess {public get; public set;}
    public List<SelectOption> FilterTypes {public get; private set;}
    public List<SelectOption> Processes {public get; private set;}
    public Boolean IsFeatureEnabled {public get; private set;}

    public PurgeLogsController(ApexPages.StandardSetController stdController)
    {
        standardController = stdController;
        
        Features.DisableDML = true;
        IsFeatureEnabled = Features.isActive(Logging.FEATURE_NAME);
        Features.DisableDML = false;
        if(!IsFeatureEnabled)
            return;
        
        Records = standardController.getRecords();
        SelectedRecords = Records.clone();
        
        FilterType = 'all';
        FilterTypes = new List<SelectOption>();
        FilterTypes.add( new SelectOption('all', 'All') );
        FilterTypes.add( new SelectOption('noerrors', 'Logs without errors') );
        FilterTypes.add( new SelectOption('process', 'Logs from process') );
        
        Processes = new List<SelectOption>();
        Processes.add( new SelectOption('', '-- None --') );

        Set<String> processSet = new Set<String>();
        for(Log__c log : records)
        {
            if(processSet.add(log.Process__c))
            	Processes.add( new SelectOption(log.Process__c, log.Process__c) );
        }
    }

    public PageReference updateFilter()
    {
        if(FilterType == 'all')
        {
            SelectedRecords = Records.clone();
        }
        else if(FilterType == 'noerrors')
        {
            SelectedRecords = new List<Log__c>();
            for(Log__c log : Records)
            {
                if(log.Errors__c == 0)
                    SelectedRecords.add(log);
            }
        }
        else if(FilterType == 'process')
        {
            SelectedRecords = new List<Log__c>();
            for(Log__c log : Records)
            {
                if(log.Process__c == FilterProcess)
                    SelectedRecords.add(log);
            }
        }

        return null;
    }
 
	public PageReference purge()
    {
        delete selectedRecords;
        return standardController.cancel();
    }
}
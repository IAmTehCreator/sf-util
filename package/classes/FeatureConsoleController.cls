public class FeatureConsoleController
{
    public Map<String, Features.Feature> FeatureMap {public get; private set;}
    public List<Features.Feature> VisibleFeatures {public get; private set;}
    public List<SelectOption> Packages {public get; private set;}
    public String CurrentFeature {public get; public set;}
    public String PackageFilter {public get; public set;}
    
    public FeatureConsoleController()
    {
        Features.EnableLogging = false;
        Features.DisableDML = true;
        
        Set<String> uniquePackages = new Set<String>();
        FeatureMap = new Map<String, Features.Feature>();
        VisibleFeatures = new List<Features.Feature>();
        for(Features.Feature feature : getAllFeatures())
        {
            FeatureMap.put(feature.ApiName, feature);
            feature.checkIsActive();
            if(!feature.Hidden)
            	VisibleFeatures.add(feature);
            
            uniquePackages.add(feature.HostPackage);
        }
        
        ApexPages.addMessage(new ApexPages.Message(
            ApexPages.Severity.INFO, 'Using this page you can activate and deactivate features.'
        ));
        
        Packages = new List<SelectOption>();
        Packages.add(new SelectOption('', '-- None --'));
        for(String packageName : uniquePackages)
        {
            Packages.add(new SelectOption(packageName, packageName));
        }

        Features.EnableLogging = true;
        Features.DisableDML = false;
    }
    
    public PageReference filter()
    {
        VisibleFeatures.clear();
        if(PackageFilter != '')
        {
            for(Features.Feature feature : getAllFeatures())
            {
                if(!feature.Hidden && feature.HostPackage == PackageFilter)
                    VisibleFeatures.add(feature);
            }
        }
        else
        {
            for(Features.Feature feature : getAllFeatures())
            {
                if(!feature.Hidden)
                    VisibleFeatures.add(feature);
            }
        }

        return null;
    }
    
    public PageReference toggle()
    {
        CurrentFeature = ApexPages.currentPage().getParameters().get('featurename');

        Features.Feature feature = FeatureMap.get(CurrentFeature);
        Features.FeatureActivationRequest request = feature.IsActive ? Features.canDeactivate(CurrentFeature) : Features.canActivate(CurrentFeature);
        
        if(request.isSuccess())
        {
            request = feature.IsActive ? Features.deactivate(CurrentFeature) : Features.activate(CurrentFeature);
            if(request.isSuccess())
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, CurrentFeature + ' has been toggled.'));
                feature.checkIsActive();
            }
        }
        
        if(!request.isSuccess())
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, request.getError()));

        return null;
    }
    
    private List<Features.Feature> getAllFeatures()
    {
        return Features.getAllFeatures();
    }
}